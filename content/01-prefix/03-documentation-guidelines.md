+++
title = "Documentation"
weight = 3
+++

Please follow these guidelines when writing new content or when create Merge Requests.

## Documentation Guidelines

* minimize screenshots: since UI changes happen often, we only add screenshots when absolutely necessary

* target audience: the documentation and labs are written for technical audiences with some basic CloudForms/ManageIQ knowledge. Beginners are asked to use other sources, e.g. CL220 course offered by Red Hat or start with the labs documented on [Christian Jung's homepage](http://labs.jung-christian.de)

* labs are intended to get real world experience but do not necessarily explain technical background. Refer to sources like the [ManageIQ Addendum](https://manageiq.gitbook.io/mastering-cloudforms-automation-addendum/) or the official [CloudForms Blog](http://cloudformsblog.redhat.com)

* labs are supposed to be modular, in the few exceptions where this is not possible, they should reference prerequisites in the beginning of the lab

* the labs in this repository focus on the Ansible integration and will always be based on Ansible Tower or the Embedded Ansible capabilities whenever possible
