+++
title = "Add Ansible Tower Provider"
weight = 2
+++

To be able to use Ansible Tower Jobs or Workflows in the CloudForms Service Catalog, the provider has to be added and configured first.

- Navigate to ***Automation*** -> ***Ansible Tower*** -> ***Explorer***

- Click on ***Configuration*** -> ***Add a new Provider***

- Enter the details of your Ansible Tower Server:

***Name:*** Ansible Tower

***Zone:*** Default Zone

***Url:*** tower.example.com

***Verify Peer Certificate:*** disable

***Username:*** admin

***Password:*** r3dh4t1!

{{% notice info %}}
As you probably know by know, CloudForms is executing a lot of actions in the background. After adding the Ansible Tower Provider, it can take a minute or two until all objects are retrieved and can be seen in the CloudForms UI.
{{% /notice %}}

## Review the retrieved items

After adding the Ansible Tower Provider, verify the inventory has been populated in CloudForms.

- Configured Systems: verify to see the same number of systems as in Tower

- Templates: verify to see the sane number of Tower Job Templates and Workflow Templates
