+++
title = "Store Credentials"
weight = 5
+++

To allow Ansible to log into a managed node like a Virtual Machine, the necessary credentials have to be stored in CloudForms.

## Store Virtual Machine Credentials

Ansible is using SSH by default to perform actions on the target machine. To be able to login, it has to know the login credentials.

- Navigate to ***Automation*** -> ***Ansible*** -> ***Credentials***

- Click on ***Configuration*** -> ***Add a new Credential***

- Use the following settings:

***Name:*** Virtual Machine credentials

A user descriptive name for the Credentials you want to store.

***Credential type:*** Machine

CloudForms supports several credential types to connect to other systems. For this lab we chose "Machine".

***Username:*** root

The username used to login to the target system.

***Password:*** r3dh4t1!

The password used to login to the target system.

{{% notice info %}}
Passwords are stored encrypted in the CloudForms database.
{{% /notice %}}

- Click ***Add*** to save the credentials

{{% notice info %}}
This is also an action which is preformed in the background and it can take a few seconds until you can see the new credentials in the Web UI.
{{% /notice %}}
