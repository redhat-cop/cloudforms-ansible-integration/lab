+++
title = "Dynamic Inventories"
weight = 2
+++

Ansible Tower and CloudForms are both using Inventories to allow end users to interact with objects like Virtual Machines, Hypervisors (Hosts) or Date Stores.

Although both tools can talk to infrastructure providers, it is recommended to have a single source of truth. Otherwise issues can occur triggered by out of sync data. Image a provisioning workflow triggered by CloudForms. After the initial VM was created, CloudForms will run an Ansible Tower Job to configure the new Virtual Machine. If Tower does not see the VM though (e.g. due to an out of date inventory caused by cached data), the Ansible Tower Job will fail.

A typical scenario is therefore to use CloudForms as the primary source of truth and let Ansible Tower sync its inventory from CloudForms.

On top of the obvious advantage of having inventories always in sync, it helps to reduce load on your infrastructure provider (since only CloudForms will be contacting the provider for inventory refresh). CloudForms also is more efficient since it does not do simply query the whole inventory of any given provider, but it also listens to the respective event bus, can do targeted refresh and provides the concept of Zones and Regions to help meeting scalability and high availability needs.

Last but not least, CloudForms also provides some extra data to Tower when the Job or Workflow Template is triggered. This data can be used during Playbook runs and is available as [extra var](https://docs.ansible.com/ansible/latest/user_guide/playbooks_variables.html#passing-variables-on-the-command-line).

## Configure Dynamic Inventory in Ansible Tower

Ansible Tower comes with an dynamic inventory which is able to retrieve data from CloudForms. The following steps will guide you through the process of configuring it.

### Credentials

Let's start by configuring the CloudForms log in credentials.

- navigate to ***Credentials*** in the ***Resources*** Section of the main navigation menu on the left

- click the green plus icon to add new credentials

***Name:*** CloudForms Login

***Organization:*** Default

***Credential Type:*** Red Hat CloudForms

***CloudForms URL:*** [https://cfui.example.com](https://cfui.example.com)

{{% notice note %}}
Make sure you specify https as a protocol. Ansible Tower will allow you to not specify a protocol, but will fail later when executing the inventory refresh.
{{% /notice %}}

***Username:*** admin

***Password:*** r3dh4t1!

- Click ***Save***

### Inventory

After the credentials have been saved in Ansible Tower, the inventory can be added. Since the inventory references the credentials, the order in which these items are configured, is important.

- Navigate to ***Inventories*** in the ***Resources*** section of the main menu on the left

- Click on the green plus icon to add a new inventory

- Select ***Inventory+**

- Enter the necessary details:

***Name:*** CloudForms Inventory

***Organization:*** Default

- Click ***Save***

- Click ***Sources*** on the top

- Click on the green plus icon to add a new source

***Name:*** CloudForms Source

***Source:*** Red Hat CloudForms

***Credentials:*** CloudForms Login

***Update Options:*** Check ***Overwrite*** and ***Update on Launch***

- Click on ***Save**

### Verify Dynamic Inventory

For later parts of this lab it is important to verify the Dynamic Inventory works properly.

- Navigate to ***Inventories*** in the ***Resources*** section of the main menu on the left

- Click on ***CloudForms Inventory***

- Click on ***Sources***

- Click on the reload icon on the right in the row starting with "CloudForms Source"

The icon will start flashing in green and if everything worked, will remain green.

{{% notice warning %}}
If the icon switches to red, please do not continue with this lab until you fixed the issue since subsequent parts of this lab rely on the Dynamic Inventory synchronization.
{{% /notice %}}

{{% notice info %}}
The inventory sync is a special Ansible Tower Job. You can see the output for debugging under ***Jobs*** in the ***Views*** section of the main navigation on the left.
{{% /notice %}}
