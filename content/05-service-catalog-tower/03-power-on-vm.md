+++
title = "Prepare target VM"
weight = 3
+++

The following lab will use UI customization to illustrate how easy it is to add additional functionality to CloudForms. The example will use an Ansible Playbook which will be executed on a Virtual Machine. Ansible uses SSH on Linux and WinRM on Windows to access the remote machine and therefore the VM has to be powered on.

## Power on VM

The following steps will power on a Virtual Machine which we later use as the target for the Ansible Playbook.

- Navigate to ***Compute*** -> ***Infrastructure*** -> ***Virtual Machines***

- Tiles represent the Virtual Machines. Note that the VM "cfme008" is powered off.

- Click on the tile icon "cfme008" to see the VM details.

- Click ***Power*** -> ***Power On*** to power on the Virtual Machine

- CloudForms will perform this action in the background and it will take a few minutes to complete. Click on the reload icon in the menu bar to reload the screen.

- Verify the "Power State" of the Virtual Machine has changed to "on" before you proceed with the next steps of the lab.

{{% notice note %}}
The VM should also report an IP address in the 192.168.0.0/24 network.
{{% /notice %}}

Now our test VM is up and running and we can proceed with the next steps.
