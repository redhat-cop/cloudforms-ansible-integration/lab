+++
title = "Automation State Machines with Ansible Tower"
weight = 10
chapter = true
+++

# Automation State Machines with Ansible Tower

Learn some basics of CloudForms, how to build a Service Catalog and leverage the power of Ansible.
