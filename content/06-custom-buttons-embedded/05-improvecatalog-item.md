+++
title = "Improve Service Catalog Item"
weight = 5
+++

To be able to use the new Service Dialog with our button, we first have to create an additional Service Catalog Item, which points to the Service Dialog.

## Create Service Catalog Item

- Navigate to ***Services*** -> ***Catalogs***

- Navigate to ***Catalog Items*** in the accordion on the left

- Click on ***Configuration*** -> ***Add a New Catalog Item***

- Select ***Ansible Playbook*** as Catalog Item Type

- Use the following parameters when defining the Service Catalog Item:

***Name:*** Install Package from Button

***Description:*** Install Package via Ansible Playbook

***Display in Catalog:*** Yes

***Long Description:*** &lt;empty&gt;

This is an optional field to provide a more detailed description. If you want, you can be creative here. CloudForms even supports HTML formatting which is often used to embed links to Documentation or additional resources on the selected Service Catalog Item.

***Catalog:*** My Company/Ansible

***Repository:*** GitLab

***Playbook:*** playbooks/InstallPackage.yml

***Machine Credentials:*** Virtual Machine credentials

***Variables & Default Values***: add one new entry with:

***Variable:*** package_name

***Default Value:*** httpd

{{% notice note %}}
Click the little plus ("+") icon to save the row.
{{% /notice %}}

***Dialog:*** Use Exiting

Use "Install Package from Button" as the name of the Dialog, which is the one we created in the previous step.

- Click ***Add*** to save all changes

## Update the Button definition

As the last step, we have to change the definition of our button, to point to the just created Service Catalog Item.

- Navigate to ***Automation*** -> ***Automate*** -> ***Customization***

- Click on ***Buttons*** in the accordion on the left

- Click on the "Install Package" Button you created in the previous lab

- Click on ***Configuration*** -> ***Edit this Button***

- Change the Playbook Catalog item to the new one you just created "Install Package from Button"

- Click ***Save*** to store all changes

## Test the improved Button

- Navigate to ***Compute*** -> ***Infrastructure*** -> ***Virtual Machines***

- Click on the "cfme008" tile if not already selected

- On the details page of "cfme008" click on ***Tools*** -> ***Install Package***

- Click on ***Tools*** -> ***Install Package***

{{% notice note %}}
You should see the simplified version of the Dialog. The Package Name has a better description, there is a tool tip if you hover the mouse over the little "i" icon and the redundant fields for "Machine Credentials" and "Hosts" are gone.
{{% /notice %}}

- Click ***Submit*** to execute the Button action

- Navigate to ***Services*** -> ***My Services***

- As a result of your action, a new "My Services" object was created. If you don't see it yet, wait a minute and click on the reload button.

- Click on the "Install Playbook" item to see the details

- Click on the ***Provisioning*** tab to see output from your Ansible Playbook

{{% notice optional %}}
Feel free to repeat this part of the lab with a different package name. You could use "screen" as an example instead of httpd - or some other package you want to install.
{{% /notice %}}
