+++
title = "Feedback"
weight = 4
+++

## How to provide Feedback

If you find a problem or bug in this lab, please file an issue on the [GitLab Issues](https://gitlab.com/redhat-cop/cloudforms-ansible-integration/lab/issues) page.

We're always keen to get more contributors helping us to extend the content, update instructions and help us to make this lab better. You can either file an issue or, even better, send us your Merge Request on the [GitLab Repository](https://gitlab.com/redhat-cop/cloudforms-ansible-integration/lab/).
