+++
title = "Windows Machines with Ansible Tower"
weight = 12
chapter = true
+++

# Windows Machines with Ansible Tower

Learn how to manage Windows Machines with Embedded Ansible.
