+++
title = "Create Service Catalog Item"
weight = 5
+++

Service Catalog Items are presented to the end user. A Service Catalog Item has a name, description and an optional long description where HTML formatting is supported. This allows to build user friendly Items including some eye candy or the capability to use links to reference to additional documentation.

A Service Catalog Item also provides the metadata used to describe the steps executed during an order process.

## Create a Service Catalog Item

In the following step we create a Service Catalog Item which will execute an Ansible Playbook.

- Navigate to ***Services*** -> ***Catalogs***

{{% notice note %}}
If you followed the instructions by the letter, you're already in this part of the UI.
{{% /notice %}}

- Navigate to ***Catalog Items*** in the accordion on the left

- Click on ***Configuration*** -> ***Add a New Catalog Item***

- Select ***Ansible Playbook*** as Catalog Item Type

- Use the following parameters when defining the Service Catalog Item:

    ***Name:*** Install Package

    The user friendly name of the Service Catalog Item. It will be presented to the end user.

    ***Description:*** Install Package via Ansible Playbook

    Additional description about the Service Catalog Item to make it easier for the end user to find what they are looking for.

    ***Display in Catalog:*** Yes

    You can hide Service Catalog Items from users by setting this to "No". For this lab we want to allow users to order the Service Catalog Item, so we set this to "Yes".

    ***Long Description:*** &lt;empty&gt;

    This is an optional field to provide a more detailed description. If you want, you can be creative here. CloudForms even supports HTML formatting which is often used to embed links to Documentation or additional resources on the selected Service Catalog Item.

    ***Catalog:*** My Company/Ansible

    In which Service Catalog do you want the Service Catalog Item to show up?

    ***Repository:*** GitLab

    You might have many git repositories, to better identify the correct Ansible Playbook, you first select the Repository. We only have one Repository so far, so this is simple:

    ***Playbook:*** playbooks/InstallPackage.yml

    The actual Playbook which will be executed when the Service Catalog Item is ordered:

    ***Machine Credentials:*** Virtual Machine credentials

    The credentials used to login to the target machine to run the Ansible Playbook:

{{% notice note %}}
All other fields can remain set to their respective defaults.
{{% /notice %}}

Ansible Playbooks can use variables which gives us more flexibility. In this example the package name is not hard coded, but can be set and changed from a variable:

***Variables & Default Values***: add one new entry with the following details.

Since a Playbook can have multiple variables, you can add multiple lines.

***Variable:*** package_name

***Default Value:*** httpd

{{% notice note %}}
Click the little plus ("+") icon to save the row. We only use one variable in this playbook, but your Playbooks might use more.
{{% /notice %}}

***Dialog:*** Create New

Use "InstallPackage" as the name of the Dialog. CloudForms will automatically create the Service Dialog for us, to save some time. The automatically created Service Dialog is still fully customizable, which we will do in a later part of the lab.

- Click ***Add*** to save all changes
