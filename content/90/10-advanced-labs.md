+++
title = "Advanced Labs"
weight = 10
+++

If you were able to complete all the steps and still have some time left, here are a couple of things you can do to get more familiar with CloudForms.

## Use the Self Service User Interface

The user interface we used so far is often referenced as the "Operations UI" or the "Classic UI". A new, more modern, Self Service User Interface is also available and receives improvements with every release.

The Self Service user Interface can be accessed by appending the string "self_service" to the Appliance URL.

    https://cfui-<GUID>.labs.rhpds.com/self_service

You can login with the same credentials as before.

## Build a button to execute remote commands

You have learned how to create a Service Dialog, Ansible Playbook Service Catalog Item and how to use them from a Button to extend the out of the box features provided by CloudForms. We can use this knowledge to implement a button to allow users to execute remote commands on a Virtual Machine.

This is a very popular use case because it provides a number of advantages:

- administrators can allow VM owners to run specific commands themselves, so they don't have to open a service request with IT
- administrators can also use CloudForms' powerful Role Based Access Control to make buttons only available to specific user groups
- administrators can grant end users to run commands as a privileged user (e.g. root), without handing out credentials or setting up ACL's on all managed systems
- users can run specific tasks by themselves and don't have to open an internal ticket or wait for someone with the necessary privileges to do it for them

Since this is an advanced lab, there are no detailed step by step instructions. Instead here are couple of pointers to get you on track:

#### The use case

We want end users to able to run certain remote commands by themselves. First we try this with a text box where users can enter their command - but as an additional improvement you should try to replace the text box with a drop down list, where users can only select from a list of pre approved commands.

### The Ansible Playbook

The GitLab repository used in this lab also provides a Playbook which you can use. It's called operatorcommand.yaml and requires the parameter "command". This parameter is used to specify the command which will be executed on the Virtual Machine.

### Service Dialog

Based on your work with "Install Package" Service Dialog, you should know what is needed to create a new Service Dialog for this use case.

Start with a simple copy of your working example. Add a text box to ask the user for the command. It is important that you set the name for the text element correctly.

:warning: ***WARNING*** Make sure the text box name is called "param_command" - because "command" is the name of the extra variable in the Ansible Playbook. If you chose a different name, the Playbook will use the default command, which is "echo hello world".

### Service Catalog Item

To be able to call an Ansible Playbook from a button, you need to create a Service Catalog Item first. This should be very easy for you, since you've done this before in this lab. Just make sure you run the correct playbook and use the Service Dialog you created in the previous step.

The name of the Ansible Playbook is "operatorcommand.yaml"

### Button

Create a new Button and add it to the existing Button Group "Tools" - or create a new one. Use the Service Catalog Item created in the previous step when you define the button.

### Improve it further

The advanced settings of a Button allow you to use Role Based Access Control. Check the "Role Access" option at the bottom of the settings page.

### Need Help?

Don't hesitate to ask any of the lab organizers for help, if you're stuck!

# Even more?

If you're already done and still have some time left, here are some ideas for advanced labs:

- upload icons to make the Service Catalog more appealing
