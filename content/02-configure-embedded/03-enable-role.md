+++
title = "Enable Embedded Ansible"
weight = 3
+++

This part of the lab will guide you through the process of setting up an Service Catalog Item which runs an Ansible Playbook.

## Make sure Embedded Ansible role is enabled and running

Before we continue, we want to make sure the Embedded Ansible role is enabled and running.

- Click on your user name on the top right and click on ***Configuration***

- Make sure the "Embedded Ansible" and the "Git Repositories Owner" Roles are enabled

{{% notice note %}}
We use DHCP to assign IP addresses in this lab, the IP address in the screenshot might be different from your appliance.
{{% /notice %}}

- Click on ***Diagnostics*** in the accordion on the left and click on the ***Workers*** tab

- Make sure you can see a line indicating the "Embedded Ansible Worker" is in state "started"

{{% notice note %}}
The git role is not represented by a specific worker process.
{{% /notice %}}

{{% notice warning %}}
We've noticed that sometimes the role does not start automatically. You can trigger a restart by clicking on ***Diagnostics*** -> ***Server*** and then ***Configuration*** -> ***Restart Server***. This will trigger a restart of all services and can take a few minutes to complete. Only do this, if your Embedded Ansible role was not in state "started". You will see a "Service is temporarily not available" error while the services are restarting.
{{% /notice %}}

{{% notice note %}}
We don't really need the "Git Repositories Owner" Role for this lab, but you might need it for optional tasks or advanced labs later.
{{% /notice %}}
