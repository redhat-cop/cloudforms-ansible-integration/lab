+++
title = "Windows Machines with Embedded Ansible"
weight = 11
chapter = true
+++

# Windows Machines with Embedded Ansible

Learn how to manage Windows Machines with Embedded Ansible.
