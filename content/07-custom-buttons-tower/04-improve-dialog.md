+++
title = "Improve Service Dialogs"
weight = 7
+++

The automatically generated Service Dialog is far from being perfect. It might confuse the user with too many input fields. For example, the user is asked to select the "Machine Credentials", although they have already been defined in the Service Catalog Item and the user provided input will have no effect. It also asks for the "Host", but this one is automatically calculated by CloudForms and set to the currently selected Virtual Machine. And finally the field "package_name" could benefit from a more descriptive text to be more user friendly.

In the following steps, we want to make the Service Dialog more user friendly by simplifying it.

## Edit the Service Dialog

- Navigate to ***Automation*** -> ***Automate*** -> ***Customization***

- Click on ***Service Dialogs*** in the accordion on the left

- Click the check box next to "InstallPackage"

- Click on ***Configuration*** -> ***Copy the selected Dialog to a new Dialog***

- Let's improve the Service Dialog by applying the following changes:

***Dialog's name:*** Install Package from Button

- Hide the element "Service Name" by clicking on the little pen icon.

{{% notice note %}}
The edit icon only shows if you move the mouse pointer over the "Service Name" text box.
{{% /notice %}}

{{% notice warning %}}
Do not delete the element, only hide it! The element is still needed for some CloudForms internal logic and should not be removed.
{{% /notice %}}

- Click on ***Options*** and switch the ***Visible*** switch to "No"

- Click ***Save*** to close the dialog

- Repeat this for the "Limit" element. Click on the pen icon next to it.

{{% notice note %}}
The edit icon only shows if you move the mouse pointer over the "Hosts" text box.
{{% /notice %}}

{{% notice warning %}}
Do not delete the element, only hide it! The element is still needed for some CloudForms internal logic and should not be removed.
{{% /notice %}}

- Click on ***Options*** and switch ***Visible*** to "No"

- Click on the little pen icon next to the "package_name" text box

{{% notice note %}}
The edit icon only shows if you move the mouse pointer over the "package_name" text box.
{{% /notice %}}

- Change the label to something more descriptive:

***Label:*** Enter Package Name

{{% notice warning %}}
Do not change the field "Name" - it is the name of the variable used internally by CLoudForms and the Ansible Playbook. if you change the name of this field, the Playbook will not pickup the new variable and hence ignore the user input.
{{% /notice %}}

- Also let's give more information to the user by improving the "Help" text:

***Help:*** Enter the name of the RPM package to be installed on the system

- Click on ***Options*** and switch ***Read only*** to "No"

{{% notice info %}}
This makes the field editable to the end user when filling out the Service Dialog.
{{% /notice %}}

- Click ***Save*** to apply the changes

- Click ***Save*** To save all changes we made in the Service Dialog

## Reconfigure the Custom Button

We have to edit the Custom Button to use the new Service Dialog.

- Navigate to ***Automation*** -> ***Automate*** -> ***Customization***

- Click on ***Buttons*** in the accordion on the left

- Navigate to the Button you created before called "Install Package" in the "Ansible Tower" button group

- Click on ***Configuration*** -> ***Edit this Button***

- Change:

***Dialog:*** Install Package from Button

- Click ***Save***

## Verify the improved Service Dialog

Finally we want to make sure the changes became effective and work as expected.

- Navigate to ***Compute*** -> ***Infrastructure*** -> ***Virtual Machines***

- Navigate to "cfme008"

- Click on ***Ansible Tower*** -> ***Install Package***

You should notice the "Service Name" and "Limit" field are gone and the "Enter Package Name" is not editable.

- Provide a package name which is part of RHEL (for example mariadb) and click ***Submit***

Verify the Job was executed successfully.
