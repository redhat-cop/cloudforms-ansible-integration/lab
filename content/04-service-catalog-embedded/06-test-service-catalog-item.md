+++
title = "Test Service Catalog"
weight = 6
+++

Order the previously created Service Catalog Item from the Service Catalog to verify it was created correctly.

## Test the Service Catalog Item

We want to make sure the resulting Service Catalog Item actually works.

- Navigate to ***Services*** -> ***Catalogs***

- Click on ***Service Catalogs*** in the accordion on the left, if not already selected

- Select the "Install Package" Service Catalog Item

- Click ***Order***

- Select the following options:

***Machine Credentials:*** Virtual Machine Credentials

These are the credentials stored in CloudForms earlier, to log into the target machine.

***Hosts:*** localhost (should already be the default)

On which machine the Playbook should be executed.

***package_name:*** httpd (should already be the default)

The variable specified when creating the Service Catalog Item, which can be overriden by the end user during order.

- Click on ***Submit***

- After submitting your order, you will be redirected to the Requests Queue. You should also see pop up notifications on the top right informing you about the progress of your order.

{{% notice optional %}}
Click on ***Refresh*** to monitor the progress of your order
{{% /notice %}}

- Navigate to ***Services*** -> ***My Services***

- Every time a user places an order a object under "My Services" is created. You should see one tile labeled "Install Package"

- Click on the tile icon to get more details

- Click on the tab ***Provisioning*** to see details of the Ansible Playbook run

{{% notice note %}}
In this example the Playbook completed successfully. In your case it might be still running and not be complete. Click the little reload icon on the page to reload the information while the Playbook is executed in the background.
{{% /notice %}}
