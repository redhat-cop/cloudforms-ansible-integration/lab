+++
title = "Create Custom Button"
weight = 2
+++

CloudForms can easily be extended by adding additional menus and buttons. This allows seamless integration of customization and making them available to end users.

## Add a Button Group

To add new button to the UI, we first need to create a Button Group. A Button Group is basically a new menu entry in the UI. Buttons and Button Groups can be assigned to several objects in CloudForms.

- Navigate to ***Automation*** -> ***Automate*** -> ***Customization***

- Click on ***Buttons*** in the accordion on the left

- Click on ***VM and Instance***

- Click on ***Configuration*** -> ***Add a new Button Group***

- Enter the following data into the form:

***Text:*** Ansible Tower Tools

The name of the Button Group, or menu, as shown in the UI.

***Hover Text:*** Additional tasks

A description text which will be shown when hovering the mouse over the Button Group.

***Icon:*** search for the wrench symbol in ***Font Awesome***

An icon for the Button Group.

- Click ***Add*** to create the button group

In the next section we will add a button to the group.

## Add a new Button to the Button Group

The previous step created a Button Group, or menu. Now we want add Buttons to the Group:

- Navigate to ***Automation*** -> ***Automate*** -> ***Customization***

{{% notice note %}}
You should already be in this menu if you followed the previous steps
{{% /notice %}}

- Click on the "Tools" Button Group you created in the previous lab

- Click on ***Configuration*** -> ***Add a new Button***

- Make the following adjustments:

***Button Type:*** Default

***Text:*** Install Package

***Hover Text:*** Install Package via Ansible Playbook

Click on the Icon symbol and choose an Icon of your liking.

***Dialog:*** Install Package

Leave the other settings on this page unmodified and switch to ***Advanced***.

Scroll down to the section "Object Details" and apply the following settings:

***System/Process:*** Request

***Message:*** Create

***Request:*** ansible_tower_job

Under "Attribute/Value Pairs" add the following rows:

- Row 1: job_template_name / InstallPackage

- Row 2: param_package_name / bash

{{% notice note %}}
It's important to use the correct spelling or the Job will fail.
{{% /notice %}}

{{% notice info %}}
Everything prefixed with "param_" will be sent as an extra variable to Ansible Tower, when executing the Job. In this example above the extra variable "package_name" with the value "httpd" will be used when executing the Ansible Tower Job.
{{% /notice %}}

{{% notice info %}}
CloudForms will automatically send a limit parameter with the currently selected Virtual Machine or Instance. This will make sure the Playbook will not be executed on the entire inventory but only on the intended VM.
{{% /notice %}}

## Verify the Custom Button

We want to make sure the Custom Button actually works and executes the configured Ansible Tower Job.

- Navigate to ***Compute*** -> ***Infrastructure*** -> ***Virtual Machines***

- Click on ***cfme008***

- Click on the new menu on the top called ***Ansible Tower*** and the entry ***Install Package***

{{% notice note %}}
You will notice the "package_name" field is disabled and you can not change "httpd" to something else. In a later part of this lab, you will learn how to change that.
{{% /notice %}}

- Click on ***Submit*** to confirm the action

{{% notice tip %}}
You might have noticed that the package name specified in the Dialog is "httpd" but when we created the button, we set the package name to "bash". What do you think, which package will be installed? httpd? bash? none of the above?
{{% /notice %}}

After the Playbook was executed, you can find the Ansible Tower Job results in ***Automation*** -> ***Ansible Tower*** -> ***Jobs***. In this release of CloudForms you can not see the Ansible Playbook output. This is planned for a future release of CloudForms.
