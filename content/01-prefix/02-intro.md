+++
title = "Introduction"
weight = 2
+++

This chapter will give you some context of the intention of writing these labs, on how to use them and how to provide feedback.

## How it all started

There are several places on the internet to find information about [CloudForms](http://www.redhat.com/cloudofrms/) and [ManageIQ](http://wwww.manageiq.org). A good start is of course the [CloudForms Product Documentation](TODO LINK) or the [ManageIQ Project Documentation](TODO LINK). The [Red Hat CloudForms Blog](http://cloudformsblog.redhat.com) provides additional details and articles on how to use CloudForms. There is also a [ManageIQ Blog](TODO LINK) to follow news of the upstream project.

To learn everything there is to know about Automate, Peter McGowan's [Mastering CloudForms Automate](https://pemcg.gitbooks.io/mastering-automation-in-cloudforms-4-2-and-manage/content/) and the [Addendum](https://manageiq.gitbook.io/mastering-cloudforms-automation-addendum/) are great resources.

On top of that you can find articles on [Christian Jung's Blog](http://www.jung-christian.de) or the [TigerIQ Blog](http://tigeriq.co).

What is missing though is a consolidated source for Ansible related topics. Although the Ansible integration is improving with each release and constantly becomes easier to use, there are certain advanced topics which are not very well documented. It's also often useful to have all information in a consolidated sources.

This Lab is an attempt to provide such documentation. There is a series of blog posts which give you the theoretical background to understand how Ansible and CloudForms work together (TODO LINK). This collection of labs will give you the hands on experience to use the theoretical knowledge in real world scenarios.

## How to access the lab environment

TODO: Instructions for RHPDS

## How to build your own lab environment

Prerequisites:

* CloudForms Appliance with at least one Infrastructure or Cloud Provider

* Internet access from CloudForms to use git repositories

* Ansible Tower for the Tower related labs (or AWX?)
