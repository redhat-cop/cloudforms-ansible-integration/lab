+++
title = "Create Service Catalog"
weight = 4
+++

To offer a Service Catalog Item to users, they have to be organized in Service Catalogs.

## Create an Ansible Service Dialog

If the Job Template in Tower is well specified, CloudForms can automatically generate a [Service Dialog](/99-glossary/02-list#service-dialog) from it. For this to work, the extra variables required to run an Ansible Tower Job have to be configured as part of the Job Template Definition. When done correctly, the variables will show up in the CloudForms UI as a property of the Ansible Tower Job Template.

- Navigate to ***Automation*** -> ***Ansible Tower*** -> ***Explorer***

- Click on ***Templates*** in the accordion on the left

- Click on the ***InstallPackage*** Job Template

- Note the variable "package_name" with its default value "httpd" in the "Variables" section of the UI

- Click on ***Configuration*** -> ***Create Service Dialog from this Template***

***Service Dialog Name:*** InstallPackage

- A notification will indicate that the dialog was created

{{% notice tip %}}
You can verify the automatically generated Service Dialog by navigating to ***Automation*** -> ***Automate*** -> ***Customization***. Click on ***Service Dialogs*** in the accordion on the left and you should see the automatically generated one. In a later part of this lab, you will learn how to modify a Service Dialog to make it more user friendly.
{{% /notice %}}

## Create an Ansible Service Catalog

This part of the lab will guide you through the process of creating a Service Catalog.

- The next step is to create a Service Catalog. First we have to navigate to ***Services*** -> ***Catalogs***.

- On this screen click on ***Catalogs*** on the left

- Click on ***Configuration*** and ***Add a New Catalog***

- Fill out name and description:

***Name:*** Ansible Tower

A user friendly name of the Service Catalog. End users will see the different Service Catalogs by name.

***Description:*** Order Ansible Playbooks from a Service Catalog

Additional description about the Service Catalog. End users will see the description and it will help them to find the Service Catalog Items they are looking for.

- Click on ***Add*** to save the new Catalog

## Create a Service Catalog Item

In the following step we create a Service Catalog Item which will execute an Ansible Playbook.

- Navigate to ***Services*** -> ***Catalogs***

{{% notice note %}}
If you followed the instructions by the letter, you're already in this part of the UI.
{{% /notice %}}

- Navigate to ***Catalog Items*** in the accordion on the left

- Click on ***Configuration*** -> ***Add a New Catalog Item***

***Catalog Item Type:*** Ansible Tower

***Name:*** Install Package

***Description:*** Install Package via Ansible Playbook

***Display in Catalog:*** yes

***Catalog:*** My Company/Ansible Tower

***Dialog:*** InstallPackage

***Provider:*** Ansible Tower

***Ansible Tower Template:*** InstallPackage

Leave the "Provisioning Entry Point" as it is or otherwise the hook into the backend logic is broken and the order will fail to execute.

{{% notice tip %}}
On the "Details" tab additional information can be provided for the end user when ordering this Service Catalog Item. You can use simple HTML formatting in this field as well.
{{% /notice %}}

## Verify Service Catalog Item

We want to test the Service Catalog Item works as expected.

- Navigate to ***Services*** -> ***Service Catalog***

- Click on ***Service Catalogs*** in the accordion on the left

- Click on ***InstallPackage*** in the ***Ansible Tower*** Service Catalog

- Click on ***Order***

***Service Name:*** Ansible Tower Test

***Limit:*** cfme009

***package_name:*** httpd

{{% notice info %}}
The package_name field might be read only. This will be changed in a separate part of this lab.
{{% /notice %}}

- Click ***Submit***

You will be redirected to the "Requests" queue where you can monitor the progress of your order. As a result of the order a new "My Services" object was created. It contains some metadata and the Ansible Tower Job Output.

- Navigate to ***Services*** -> ***My Services***

There should be only one object at this point in time - if there are multiple, try to find the latest one.

- Click on the Icon representing the Service

- Click on the ***Job*** Tab to see the Ansible Playbook output

Debug any problems you see, if there are any. Later parts of this lab will rely on a working Service Catalog Item and you should not proceed until all issues have been fixed.
