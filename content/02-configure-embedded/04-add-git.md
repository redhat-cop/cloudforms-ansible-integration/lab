+++
title = "Git Repository for Playbooks"
weight = 4
+++

Ansible Playbooks are usually stored in Git repositories. This allows easier collaboration, provides version control and advanced features like branching and tagging.

## Add a Git repository of Ansible Playbooks

To be able to run Ansible Playbooks, they have to become available in CloudForms. Custom git repositories can be used as well as GitHub, GitLab or others. Other Source Control Management Systems like Subversion or Mercurial are planned for later versions.

- Navigate to ***Automation*** -> ***Ansible*** -> ***Repositories***.

- Click on ***Configuration*** -> ***Add New Repository***

{{% notice warning %}}
If the menu item "Add New Repository" is disabled, the Embedded Ansible Role is not active.
{{% /notice %}}

- Fill in the form.

***Name:*** GitLab

An internal name for the git repository.

***Description:*** Example Playbooks

A description for the git repository.

***URL:*** [https://gitlab.com/cjung/partner-conference-2019.git](https://gitlab.com/cjung/partner-conference-2019.git)

How to access the git repository.

***SCM Update Options:*** check "Update on Launch"

Update on Launch causes CloudForms to check for new Playbooks or updated Playbooks before a Playbook is launched.

- Click on ***Add*** to save the settings

{{% notice note %}}
It takes a few seconds for the action to complete. A pop up notification will inform you after the task was completed.
{{% /notice %}}

You can click on your username in the top right corner and then on ***Tasks*** to see all currently running tasks. Switch to ***All Tasks*** to see the progress of your Repository import.

## Verify the task completed successfully

- Navigate back to ***Automation*** -> ***Ansible*** -> ***Repositories***.

- Click on the ***Reload*** icon to refresh the screen. After the initial import completed, you will see the list of available repositories.

- Click on the repository to see the details.

- Click on ***Playbooks*** to see the list of automatically imported playbooks.

This confirms that all playbooks have been imported successfully.
