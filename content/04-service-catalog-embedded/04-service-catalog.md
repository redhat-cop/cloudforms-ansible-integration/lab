+++
title = "Create Service Catalog"
weight = 2
+++

To offer a Service Catalog Item to users, they have to be organized in Service Catalogs.

## Create an Ansible Service Catalog

This part of the lab will guide you through the process of creating a Service Catalog.

- The next step is to create a Service Catalog. First we have to navigate to ***Services*** -> ***Catalogs***.

- On this screen click on ***Catalogs*** on the left

- Click on ***Configuration*** and ***Add a New Catalog***

- Fill out name and description:

***Name:*** Ansible

A user friendly name of the Service Catalog. End users will see the different Service Catalogs by name.

***Description:*** Order Ansible Playbooks from a Service Catalog

Additional description about the Service Catalog. End users will see the description and it will help them to find the Service Catalog Items they are looking for.

- Click on ***Add*** to save the new Catalog

## Create a Service Catalog Item

In the following step we create a Service Catalog Item which will execute an Ansible Playbook.

- Navigate to ***Services*** -> ***Catalogs***

{{% notice note %}}
If you followed the instructions by the letter, you're already in this part of the UI.
{{% /notice %}}

- Navigate to ***Catalog Items*** in the accordion on the left

- Click on ***Configuration*** -> ***Add a New Catalog Item***

- Select ***Ansible Playbook*** as Catalog Item Type

- Use the following parameters when defining the Service Catalog Item:

***Name:*** Install Package

The user friendly name of the Service Catalog Item. It will be presented to the end user.

***Description:*** Install Package via Ansible Playbook

Additional description about the Service Catalog Item to make it easier for the end user to find what they are looking for.

***Display in Catalog:*** Yes

You can hide Service Catalog Items from users by setting this to "No". For this lab we want to allow users to order the Service Catalog Item, so we set this to "Yes".

***Long Description:*** &lt;empty&gt;

This is an optional field to provide a more detailed description. If you want, you can be creative here. CloudForms even supports HTML formatting which is often used to embed links to Documentation or additional resources on the selected Service Catalog Item.

***Catalog:*** My Company/Ansible

In which Service Catalog do you want the Service Catalog Item to show up?

***Repository:*** GitLab

You might have many git repositories, to better identify the correct Ansible Playbook, you first select the Repository. We only have one Repository so far, so this is simple.

***Playbook:*** playbooks/InstallPackage.yml

The actual Playbook which will be executed when the Service Catalog Item is ordered.

***Machine Credentials:*** Virtual Machine credentials

The credentials used to login to the target machine to run the Ansible Playbook.

{{% notice note %}}
All other fields can remain set to their respective defaults.
{{% /notice %}}

Ansible Playbooks can use variables which gives us more flexibility. In this example the package name is not hard coded, but can be set and changed from a variable:

***Variables & Default Values***: add one new entry with the following details.

Since a Playbook can have multiple variables, you can add multiple lines.

***Variable:*** package_name

***Default Value:*** httpd

{{% notice note %}}
Click the little plus ("+") icon to save the row. We only use one variable in this playbook, but your Playbooks might use more.
{{% /notice %}}

***Dialog:*** Create New

Use "InstallPackage" as the name of the Dialog. CloudForms will automatically create the Service Dialog for us, to save some time. The automatically created Service Dialog is still fully customizable, which we will do in a later part of the lab.

- Click ***Add*** to save all changes

## Test the Service Catalog Item

We want to make sure the resulting Service Catalog Item actually works.

- Navigate to ***Services*** -> ***Catalogs***

- Click on ***Service Catalogs*** in the accordion on the left, if not already selected

- Select the "Install Package" Service Catalog Item

- Click ***Order***

- Select the following options:

***Machine Credentials:*** Virtual Machine Credentials

These are the credentials stored in CloudForms earlier, to log into the target machine.

***Hosts:*** localhost (should already be the default)

On which machine the Playbook should be executed.

The variable specified when creating the Service Catalog Item, which can be overriden by the end user during order:

***package_name:*** httpd (should already be the default)

- Click on ***Submit***

- After submitting your order, you will be redirected to the Requests Queue. You should also see pop up notifications on the top right informing you about the progress of your order.

{{% notice optional %}}
Click on ***Refresh*** to monitor the progress of your order
{{% /notice %}}

- Navigate to ***Services*** -> ***My Services***

- Every time a user places an order a object under "My Services" is created. You should see one tile labeled "Install Package"

- Click on the tile icon to get more details

- Click on the tab ***Provisioning*** to see details of the Ansible Playbook run

{{% notice note %}}
In this example the Playbook completed successfully. In your case it might be still running and not be complete. Click the little reload icon on the page to reload the information while the Playbook is executed in the background.
{{% /notice %}}
